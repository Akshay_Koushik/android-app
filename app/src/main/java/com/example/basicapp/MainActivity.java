package com.example.basicapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


//import static android.widget.Toast.makeText;

public class MainActivity extends AppCompatActivity {
    private TextView res;
    private RequestQueue rQ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        res = findViewById(R.id.textView);
        Button btn = findViewById(R.id.button);

        rQ = Volley.newRequestQueue(this);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                parseFn();
            }
        });
    }
        private void parseFn()
        {
            String url = "https://api.myjson.com/bins/kp9wz";
            JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET, url, null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            JSONArray arr = null;
                            try {
                                arr = response.getJSONArray("employees");

                                for (int i = 0; i < arr.length(); i++)
                                {
                                    JSONObject temp = arr.getJSONObject(i);

                                    String name = temp.getString("firstname");
                                    String mail = temp.getString("mail");
                                    int age = temp.getInt("age");

                                    res.append(name + " , " + String.valueOf(age) + " , " + mail + "\n");
                                }
                            }
                            catch (JSONException e)
                            {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            error.printStackTrace();
                        }
                    });
            rQ.add(req);
        }
    public void fn(String s)
    {
        Toast t = Toast.makeText(this,s,Toast.LENGTH_LONG);
        t.show();
    }

    public void btn(View v)
    {
        fn("Hello");
    }
}
